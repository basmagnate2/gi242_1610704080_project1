﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class UIManager : MonoSingleton<UIManager>
    {
        public static UIManager Instance { get; private set; }

        [SerializeField] private GameObject resultDialog;
        [SerializeField] private TextMeshProUGUI scoreText;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            DontDestroyOnLoad(this);
        }
        public void Start()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;

        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("LOAD");
            if (GameManager.Instance.currentScene == 2)
            {
                scoreText = GameObject.FindWithTag("ScoreText").GetComponent<TextMeshProUGUI>();
                resultDialog = GameObject.FindWithTag("ResultDialog");
                scoreText.gameObject.SetActive(false);
                resultDialog.gameObject.SetActive(false);
            }
        }

        public void OnRestartGame()
        {
            resultDialog.gameObject.SetActive(true);
            scoreText.text = "High Score : " + ScoreManager.Instance.Score.ToString();
        }

    }
}
